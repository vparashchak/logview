import sublime, sublime_plugin
import threading
import unicodedata

class StatusAnimation:
	animation = [ "...  ", " ... ", "  ...", " ... "]

	animationPos = 0
	stopFlag = False
	view = None
	key = None
	updateLock = threading.RLock()

	def updateAnimation(self):
		if (self.stopFlag):
			self.view = None
			self.key = None
		else:
			with self.updateLock:
				if (self.stopFlag == False):
					self.view.set_status(self.key, self.prefix + self.animation[self.animationPos])
			self.animationPos += 1
			if (self.animationPos >= len(self.animation)): self.animationPos = 0
			sublime.set_timeout(self.updateAnimation, 500)

	def start(self, view, prefix, delay, key):
		# Show the first animation step and wait for the given time before starting the animation
		view.set_status(key, prefix + self.animation[0])

		self.animationPos = 1
		self.stopFlag = False
		self.view = view
		self.key = key
		self.prefix = prefix
		sublime.set_timeout(self.updateAnimation, delay)

	def stop(self):
		with self.updateLock:
			self.stopFlag = True
			self.view.erase_status(self.key)

class LogView:
	regionStyles = {
		"fill": 0,
		"outline": sublime.DRAW_NO_FILL,
		"underline": sublime.DRAW_NO_FILL | sublime.DRAW_NO_OUTLINE | sublime.DRAW_SOLID_UNDERLINE,
		"none": sublime.DRAW_NO_FILL | sublime.DRAW_NO_OUTLINE
	}

	regexPrefix = "(?<![\\w])("
	regexSuffix = ")(?![\\w])"

	def highlight(self, view, regex, regionName, scope, icon, regionFlags):
		if (regex == "") or (regex == None):
			return []

		foundRegions = view.find_all(regex, sublime.IGNORECASE, None, None)
		numFoundRegions = len(foundRegions)

		if (numFoundRegions > 0):
			# Expand all regions to match the whole line and mark the line with the given scpe
			for i in range(0, numFoundRegions):
				foundRegions[i] = view.line(foundRegions[i])
			view.add_regions(regionName, foundRegions, scope, "Packages/LogView/" + icon + ".png", regionFlags);

		return foundRegions

	def markupView(self, view, statusAnimation):
		settings = sublime.load_settings("logview.sublime-settings")

		errorRegex = settings.get("error_filter", "\\[ERRO\\]|\\[ERROR\\]")
		errorScope = settings.get("error_scope", "message.error")
		errorStatusCaption = settings.get("error_status_caption", "Errors")

		warningRegex = settings.get("warning_filter", "\\[WARN\\]")
		warningScope = settings.get("warning_scope", "markup.bold")
		warningStatusCaption = settings.get("warning_status_caption", "Warnings")

		bleRegex = settings.get("ble_filter", "\\[BTLE\\]|\\[RSDK\\]")
		bleScope = settings.get("ble_scope", "markup.inserted")
		bleStatusCaption = settings.get("ble_status_caption", "BLE")

		mqttRegex = settings.get("mqtt_filter", "\\[MQTT\\]")
		mqttScope = settings.get("mqtt_scope", "markup.deleted")
		mqttStatusCaption = settings.get("mqtt_status_caption", "MQTT")

		userRegex = settings.get("user_filter", "\\[USER\\]")
		userScope = settings.get("user_scope", "markup.changed")
		userStatusCaption = settings.get("user_status_caption", "USER")

		restRegex = settings.get("rest_filter", "\\[REST\\]")
		restScope = settings.get("rest_scope", "markup.quote")
		restStatusCaption = settings.get("rest_status_caption", "REST")

		dfuRegex = settings.get("dfu_filter", "\\[DFUM\\]")
		dfuScope = settings.get("dfu_scope", "markup.heading")
		dfuStatusCaption = settings.get("dfu_status_caption", "DFU")

		appRegex = settings.get("app_filter", "application!")
		appScope = settings.get("app_scope", "markup.other")
		appStatusCaption = settings.get("app_status_caption", "App events")
		highlighStyle = settings.get("highlight_style", "fill")
		autoMatchWords = settings.get("auto_match_words", True)

		# If auto_match_words is set to true, extend the regular expressions with lookarounds to only match words.
		if (autoMatchWords):
			errorRegex = self.regexPrefix + errorRegex + self.regexSuffix
			warningRegex = self.regexPrefix + warningRegex + self.regexSuffix
			bleRegex = self.regexPrefix + bleRegex + self.regexSuffix
			mqttRegex = self.regexPrefix + mqttRegex + self.regexSuffix
			userRegex = self.regexPrefix + userRegex + self.regexSuffix
			restRegex = self.regexPrefix + restRegex + self.regexSuffix
			dfuRegex = self.regexPrefix + dfuRegex + self.regexSuffix
			appRegex = self.regexPrefix + appRegex + self.regexSuffix

		# Determin the falgs to set on the region for correct highlighting
		if (highlighStyle in self.regionStyles):
			regionFlags = self.regionStyles[highlighStyle]
		else:
			regionFlags = self.regionStyles["outline"]

		foundRegions = self.highlight(view, mqttRegex, "logfile.mqtt", mqttScope, "", regionFlags)
		view.set_status("logview.7", mqttStatusCaption + " " + str(len(foundRegions)))
		bookmarks = foundRegions
		foundRegions = self.highlight(view, appRegex, "logfile.app", appScope, "", regionFlags)
		view.set_status("logview.6", appStatusCaption + " " + str(len(foundRegions)))
		bookmarks = foundRegions
		foundRegions = self.highlight(view, dfuRegex, "logfile.dfu", dfuScope, "", regionFlags)
		view.set_status("logview.5", dfuStatusCaption + " " + str(len(foundRegions)))
		bookmarks = foundRegions
		foundRegions = self.highlight(view, restRegex, "logfile.rest", restScope, "", regionFlags)
		view.set_status("logview.4", restStatusCaption + " " + str(len(foundRegions)))
		bookmarks = foundRegions
		foundRegions = self.highlight(view, userRegex, "logfile.user", userScope, "", regionFlags)
		view.set_status("logview.3", userStatusCaption + " " + str(len(foundRegions)))
		bookmarks = foundRegions
		foundRegions = self.highlight(view, bleRegex, "logfile.ble", bleScope, "", regionFlags)
		view.set_status("logview.2", bleStatusCaption + " " + str(len(foundRegions)))
		bookmarks = foundRegions
		foundRegions = self.highlight(view, warningRegex, "logfile.warnings", warningScope, "", regionFlags)
		view.set_status("logview.1", warningStatusCaption + " " + str(len(foundRegions)))
		bookmarks += foundRegions
		foundRegions = self.highlight(view, errorRegex, "logfile.errors", errorScope, "", regionFlags)
		view.set_status("logview.0", errorStatusCaption + " " + str(len(foundRegions)))
		bookmarks += foundRegions
		del foundRegions

		# Set a bookmark for each region
		view.add_regions("bookmarks", bookmarks, "bookmarks", "", sublime.HIDDEN);
		del bookmarks

		# Stop the animation
		statusAnimation.stop()
		statusAnimation = None

		# Set the final message
		sublime.status_message("")

	# Called to prepare the view:
	# Applies the highlighting
	def prepareView(self, view):
		# Set a temporary message
		statusAnimation = StatusAnimation()
		statusAnimation.start(view, "Parsing", 2000, "0")
		#sublime.status_message("Processing log...")

		# Do the markup processing in its own thread.
		threading.Thread(target = self.markupView, daemon = True, args=[view, statusAnimation], kwargs={}).start()

	# Called to remove the preparations from the log file and turn it into a normal view.
	def unprepareView(self, view):
		view.erase_status("logview")
		view.erase_status("logview.0")
		view.erase_status("logview.1")
		view.erase_status("logview.2")
		view.erase_status("logview.3")
		view.erase_status("logview.4")
		view.erase_status("logview.5")
		view.erase_status("logview.6")
		view.erase_status("logview.7")
		view.erase_regions("logfile.errors")
		view.erase_regions("logfile.warnings")
		view.erase_regions("logfile.ble")
		view.erase_regions("logfile.user")
		view.erase_regions("logfile.rest")
		view.erase_regions("logfile.dfu")
		view.erase_regions("logfile.app")
		view.erase_regions("logfile.mqtt")
		view.erase_regions("bookmarks")

class EventListener(LogView, sublime_plugin.EventListener):
	# Called when a file is finished loading.
	def on_load(self, view):
		if (view.settings().get('syntax') == "Packages/LogView/logview.tmLanguage"):
			self.prepareView(view)

	# Called if a new view is created from an existing one. We've to check again if this view contains a logfile.
	def on_clone(self, view):
		if (view.settings().get('syntax') == "Packages/LogView/logview.tmLanguage"):
			self.prepareView(view)

	# Called if a text command is executed on the buffer
	def on_text_command(self, view, command_name, args):
		if (command_name == "set_file_type"):
			currentSyntax = view.settings().get('syntax')
			newSyntax = args["syntax"]

			if (newSyntax != currentSyntax):
				if (newSyntax == "Packages/LogView/logview.tmLanguage"):
					# If the new syntax is logview then prepare the view.
					self.prepareView(view)
				else:
					# If the old syntax was logview then remove our preparations
					if (view.settings().get('syntax') == "Packages/LogView/logview.tmLanguage"):
						self.unprepareView(view)

		# Always run the command as is
		return None

class LogViewRescan(LogView, sublime_plugin.TextCommand):
	def run(self, edit):
		if (self.view.settings().get('syntax') == "Packages/LogView/logview.tmLanguage"):
			self.unprepareView(self.view)
			self.prepareView(self.view)

	def is_enabled(self):
		return self.view.settings().get('syntax') == "Packages/LogView/logview.tmLanguage"